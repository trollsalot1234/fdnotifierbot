"""
FDNotifier by /u/trollsalot1234 as requested by /u/iSunMonkey
 
Posts a notification to each request post in /r/FreeDesign

"""
from __future__ import print_function # to print to stderr

import praw, bmemcached, time, os     # for all bots
from requests import HTTPError        # to escape ban errors
import sys                            # for all errors
import re                              # if you want to use regex
import textwrap              # if you want to post multiline comments
import xml.sax.saxutils as saxutils   # for unescaping post title

reddit = praw.Reddit("FDNotifier 1.0.0 by /u/trollsalot1234")
reddit.login(os.environ['REDDIT_USERNAME'], os.environ['REDDIT_PASSWORD'])

already = bmemcached.Client((os.environ['MEMCACHEDCLOUD_SERVERS'],), 
                             os.environ['MEMCACHEDCLOUD_USERNAME'],
                             os.environ['MEMCACHEDCLOUD_PASSWORD'])
 
subreddits = ['FreeDesign']

my_comment = "
Hi,

This is some pretty useful text right here.

I bet if you modified it you could do great things.

* I am a bot, please don't hate me.  It's not my fault /u/trollsalot1234 made me this way.
"
POSTLIMIT = 50

submissions = reddit.get_subreddit('+'.join(subreddits)).get_new(POSTLIMIT)

for s in submissions:

  title = saxutils.unescape(s.title.encode("utf-8")).lower()
  cid = str(s.id)

  if "[request]" not in title:
    continue
  if already.get(cid):
    continue

  try:
    s.add_comment(my_comment)
    already.set(cid, "True")

  # If you are banned from a subreddit, reddit throws a 403 instead of a helpful message :/
  except HTTPError as err:
    log(logging.ERROR,"Probably banned from /r/" + str(comment.subreddit), Color.RED)
   # This one is pretty rare, since PRAW controls the rate automatically, but just in case
  except praw.errors.RateLimitExceeded as err:
    log(logging.ERROR,"Rate Limit Exceeded:\n" + str(err) + " when x-posting", Color.RED)
    time.sleep(err.sleep_time)
  # If the post has already been submitted to the sub
  except praw.errors.AlreadySubmitted as err:
    log(logging.ERROR,"AlreadySubmitted error when posting.", Color.RED)      
    continue
  # Something happened and Im pretty lazy so just skip it and go to the next one
  except (AttributeError, TypeError) as err:
    log(logging.ERROR,"Attribute or Type error when posting.", Color.RED)
    continue